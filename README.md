# MindsGaming

A community website for #MindsGaming

If you are here thank you for being a member of our community!

This site layout may still need some tweaks we launched early!

(Just tag us in a post if you run into issues)

Live site:
[https://mindsgaming.glitch.me](https://mindsgaming.glitch.me)

## ![](https://cdn.glitch.com/6ca1674b-a03a-4e9c-9563-ed6203267aba%2FMindsGaming.png?v=1609603929228)

# Welcome To #MindsGaming

## Info

This website runs like a "cloud cpu", the desktop gives you access to popular groups inside our community and the backend "CPU" gives you access to our project and ablitly to cooperate with us.

This website allows our community members to co-operate with us in a newly expansive way.

You can now help us code, create folders, actively add groups, upload assets like videos, images and more with our website.

## Tips

You can go into a classic website view by clicking the settings icon (gear), and then choosing classic.

You can install or remix this website for your own use or needs like building your own community.

If a pop is stuck on a page click the "refresh" icon!

### Remix

The backend "CPU" only works with 'Glitch' to save your remix with Glitch you must create an account!
<a href="https://glitch.com/edit/?utm_content=project_mg-beta&utm_source=remix_this&utm_medium=button&utm_campaign=glitchButton#!/remix/mindsgaming">
<img src="https://cdn.glitch.com/2bdfb3f8-05ef-4035-a06e-2043962a3a13%2Fremix%402x.png?1513093958726" alt="remix this" height="33">
</a>

---

# Install (Website)

### Beta

\*Requires: <code>Node.js</code>

<code>git clone https://gitlab.com/MindsGaming/mindsgaming.git</code>

---

# Welcome To Your Folders

(This Documentation is for after remixing or cloning.)

To create a new file oor folder click "New File" in the left menu.

You must create a file to create a folder you can edit thhe file name and content later.
<code>
NewFolder/file.filename
</code>

Then create something ..

# Drag in assests!

You can drag in assests like images, music and more then use them later.

Select "assets" in the left side tab.

# Live Code

Changes to folder files wil deploy instantly refresing your project as you code, we recommend turninf theis off by clicking the project name (dropdown) and un-checking "Refresh On Changes".

<b>Do not code Live while using the "cloud CPU" website without turning off "Refresh On Changes"</b>

<b>Coding from within the site will requre the site to be refreshed to show changes</b>


## Set-Up & Customize

This "cloud cpu" only works with [Glitch](https://glitch.com), to set up edit the <code>index.html</code> where noted.

You can customize "cloud cpu" using <code>HTML</code>, <code>.js</code> , <code>markdown</code> & <code>CSS</code>

Our icon resource pack is [ipack](https://ipack.glitch.me/app)

Add more Features, "apps" and more to your "cloud CPU" and let us see the results!

## Contrubution

To contubute to this project click 'Request to Join project' above the project folders.

![](https://cdn.glitch.com/6ca1674b-a03a-4e9c-9563-ed6203267aba%2Frequest.png?v=1606944293698)
