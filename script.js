var start = document.getElementById("start");
var menu = document.getElementById("menu");
var sidemenu = document.getElementById("side-menu");
var tokens = document.getElementById("tokens");
var wallets = document.getElementById("wallets");
var traders = document.getElementById("traders");
var groups = document.getElementById("groups");
var infinity = document.getElementById("Infinity");
var splash = document.getElementById("splash");
var info = document.getElementById("info");
var rewards = document.getElementById("rewards");
var launchgroup = document.getElementById("launchgroup");
var learnhow = document.getElementById("learn-how");
var learnbtn = document.getElementById("learnBTN");

function startview() {
  closeside();
  if (start.title == "Start Here") {
    start.title = "menu";
    menu.className = "start-menu";
    tokens.className = "hide";
    wallets.className = "hide";
    infinity.className = "hide";
    groups.className = "show";
    splash.className = "hide";
  } else {
    start.title = "Start Here";
    menu.className = "hide";
    tokens.className = "hide";
    wallets.className = "hide";
    infinity.className = "hide";
    groups.className = "show";
  }
}

function tokensview() {
  if (tokens.className == "hide") {
    tokens.className = "show";
    wallets.className = "hide";
    sidemenu.className = "side-menu";
  } else {
    tokens.className = "hide";
    wallets.className = "hide";
    sidemenu.className = "hide";
  }
}

function walletsview() {
  if (wallets.className == "hide") {
    wallets.className = "show";
    tokens.className = "hide";
    sidemenu.className = "side-menu";
  } else {
    tokens.className = "hide";
    wallets.className = "hide";
    sidemenu.className = "hide";
  }
}

function tradersview() {
  if (traders.className == "hide") {
    traders.className = "show";
    wallets.className = "hide";
    tokens.className = "hide";
    sidemenu.className = "side-menu";
  } else {
    traders.className = "hide";
    tokens.className = "hide";
    wallets.className = "hide";
    sidemenu.className = "hide";
  }
}

function openhubs() {
  if (infinity.title == "Infinity") {
    infinity.title = "Hubs";
    infinity.src =
      "https://hubs.mozilla.com/EJFaEcZ?embed_token=d4f6c2c54a1a684be3668b366724fb05";
    groups.className = "hide";
    splash.className = "splash";
    infinity.width = "100%";
    infinity.height = "92%;";
    info.className = "hide";
    dreamsList.innerHTML = "Loading Hubs...";
  } else {
    infinity.title = "Infinity";
    infinity.src = "https://mtcg.glitch.me";
    groups.className = "show";
    splash.className = "hide";
    infinity.className = "hide";
    info.className = "hide";
  }
}

function chatbotview() {
  openhubs();
}

function infoview() {
  if (info.title == "Information") {
    info.title = "Info";
    info.className = "apps";
    groups.className = "hide";
    splash.className = "hide";
  } else {
    info.title = "Information";
    info.className = "hide";
    groups.className = "show";
    splash.className = "hide";
  }
}

function learnHow() {
  if (learnhow.className == "hide") {
    learnhow.className = "show";
    learnbtn.innerHTML = "Close";
  } else {
    learnhow.className = "hide";
    learnbtn.innerHTML = "Learn How";
  }
}

function rewardsview() {
  if ((rewards.title = "Closed")) {
    rewards.title = "Rewards";
    rewards.className = "splash";
    groups.className = "hide";
    splash.className = "hide";
  } else {
    rewards.title = "Closed";
    rewards.className = "hide";
    groups.className = "show";
    splash.className = "hide";
  }
}

function launchgroupview() {
  if (launchgroup.title == "Closed") {
    launchgroup.title = "Launching A Group";
    launchgroup.className = "notepad";

    groups.className = "hide";
    splash.className = "hide";
  } else {
    launchgroup.title = "Closed";
    launchgroup.className = "hide";
  }
}

function MTCGview() {
  var txt;
  if (confirm("Switch To MTCG?")) {
    window.open("https://mtcg.glitch.me", "_self");
  } else {
  }
}

function closeside() {
  tokens.className = "hide";
  wallets.className = "hide";
  groups.className = "show";
  sidemenu.className = "hide";
  traders.className = "hide";
  wallets.className = "hide";
  splash.className = "hide";
  info.className = "hide";
  rewards.className = "hide";
  launchgroup.className = "hide";
  groups.className = "show";
  infinity.title = "Infinity";
  info.title = "Information";
  rewards.title = "Closed";
  launchgroup.title = "Closed";
}

function comingsoon() {
  alert("Coming Soon!");
}

/* HTTPS */

var loc = window.location.href + "";
if (loc.indexOf("http://") == 0) {
  window.location.href = loc.replace("http://", "https://");
}
